clear all
clc
format short g

global antA antB antC
%miscela etanolo(1) e benzene(2)

antA(1)=8.04494;
antB(1)=-1554.3;
antC(1)=222.65;

antA(2)=6.90565;
antB(2)=-1211.03;
antC(2)=220.79;

AB=[2 2];
AB=fsolve('wilson',AB);
A=AB(1)
B=AB(2)

global gamma1 gamma2 P x

P=400;      %torr
x(1)=0.9;
x(2)=1-x(1);
gamma1=exp(-log(x(1)+A*x(2))+x(2)*(A/(x(1)+A*x(2))-B/(B*x(1)+x(2))));
gamma2=exp(-log(x(2)+B*x(1))+x(1)*(B/(B*x(1)+x(2))-A/(x(1)+A*x(2))));

Tb=fsolve('Tbolla',100)

Po1=10^(antA(1)+antB(1)/(antC(1)+Tb));
yetanolo=(Po1*x(1)*gamma1)/P