%fmain_Him  Esempio di equazioni risolte con fsolve_HC
%           Himmelblau equations

% R. Rota (2020)

function F=fmain_Him(x)

f(1) = 2*x(1)^3+2*x(1)*x(2)-21*x(1)+x(2)^2-7;
f(2) = x(1)^2+2*x(1)*x(2)+2*x(2)^3-13*x(2)-11;

F  = f;