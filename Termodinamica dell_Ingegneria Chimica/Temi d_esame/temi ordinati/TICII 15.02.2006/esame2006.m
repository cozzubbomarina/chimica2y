clear all
clc
close all
global p0E p G0R1 G0R2  l3
T=300; %K
p=0.5; %atm
A= 16.2243;
B = 654.84;
C = -7.16;
p0E=exp(A-B/(T+C))*1.316e-3; % [Pv in atm T in K];
G0f=[74.51 70.19 62.53 132.47 208.58];
%[A B C D E]
%[1 2 3 4 5]
G0R1=-G0f(1)-G0f(2)+G0f(4);
G0R2=-G0f(3)-G0f(4)+G0f(5);
options=optimset('maxiter',1000);
l=fsolve('sistema2006', [20 15 10],options);
nE=l3