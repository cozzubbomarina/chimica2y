function F=phi(P)
%F calcola il residuo dell'equazione: F=log(Fi_L)/log(Fi_V)-1
%dove Fi � il coefficiente di fugacit� di un liquido o vapore puro
%usata per il calcolo della pressione di saturazione imponendo 
%l'uguaglianza dei coefficienti di fugacit�

global T Tc Pc om tipo

%calcolo Z,A,B
[Z,A,B,S,k]=EoS(T,P,Tc,Pc,om,tipo);
ZV=Z(3);
ZL=Z(1);
 
%calcolo coeff fugacit� liquido e vapore con RKS (in questo caso)

logphiL=ZL-1-A/B*log((ZL+B)/ZL)-log(ZL-B);
logphiV=ZV-1-A/B*log((ZV+B)/ZV)-log(ZV-B);

F=logphiL/logphiV-1;