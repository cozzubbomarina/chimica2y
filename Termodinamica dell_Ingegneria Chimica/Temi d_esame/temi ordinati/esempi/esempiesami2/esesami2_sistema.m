function f=esesami2_sistema(l)
global n0a n0b nua nub Keq x y
l1=l(1);
l2=l(2); 
lB=l(3);
lC=l(4);
%reaz1: NB L'ATTIVITA' DEI LIQUIDI NECESSITA GAMMA
na(1)=n0a(1)+nua(1)*l1;
na(2)=n0a(2)+nua(2)*l1+lB;
na(3)=n0a(3)+nua(3)*l1+lC;
na(4)=n0a(4)+nua(4)*l1;
na(5)=n0a(5)+nua(5)*l1;
na(6)=n0a(6)+nua(6)*l1;
natot=sum(na); %mol tot presenti nel solvente S1
x=na./natot;

%reaz2:
nb(1)=n0b(1)+nub(1)*l2;
nb(2)=n0b(2)+nub(2)*l2-lB;
nb(3)=n0b(3)+nub(3)*l2-lC;
nb(4)=n0b(4)+nub(4)*l2;
nb(5)=n0b(5)+nub(5)*l2;
nb(6)=n0b(6)+nub(6)*l2;
nbtot=sum(nb); %mol tot presenti in S2
y=nb./nbtot;

%eq chimici; (ogni specie ha due attivit� diverse nelle due fasi)
gammaB1=exp(1.5*((x(3))^2));
gammaC1=exp(1.5*((x(2))^2));
gamma1=[1 gammaB1 gammaC1 1 0 0]; % nella fase alfa
att1=x.*gamma1; %vettore attivit� nela fase a
gammaB2=exp(1.5*((y(3))^2));
gammaC2=exp(1.5*((y(2))^2));
gamma2=[1 gammaB2 gammaC2 1 0 0]; %nella fase beta
att2=y.*gamma2; %vettore attivit� nela fase b

%eq fisici:
kB=exp(1.5*(((y(3))^2)-((x(3))^2)));
kC=exp(1.5*(((y(2))^2)-((x(2))^2)));

f(1)=prod((att1).^(nua))-Keq(1); %SBAGLIATOOOOO!! 
f(2)=prod((att2).^(nub))-Keq(2);
f(3)=x(2)-y(2)*kB;
f(4)=x(3)-y(3)*kC;