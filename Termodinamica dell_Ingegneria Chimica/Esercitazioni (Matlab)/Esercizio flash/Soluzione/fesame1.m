function y=fesame1(X)

global Tin Pin Po om R Tc Pc sR2 Cpv

To=X;

%calcolo sR1 (To)
    S = 0.37464+1.54226*om(2)-0.26992*om(2)^2;
    k = (1+S*(1-(To/Tc(2))^0.5))^2;
    a = 0.45724*k*R^2*Tc(2)^2 / (Pc(2)*100000);
    b = 0.07780*R*Tc(2) / (Pc(2)*100000);
    A = a*Po*100000/(R*To)^2;
    B = b*Po*100000/(R*To);
    E = S*(To/Tc(2)/k)^0.5;
    alfa = -1+B;
    beta = A-2*B-3*B^2;
    gamma = -A*B+B^2+B^3;
    coeff =[ 1
         alfa
         beta 
         gamma];
    Z = roots(coeff); %calcola le radici del polinomio
    Z = sort(Z);      %ordina in modo crescente le soluzioni
    ZV = Z(3);
    sR1 = R*(log(ZV-B)-A*E/(2*sqrt(2)*B)*log((ZV+B*(1+sqrt(2)))/(ZV+B*(1-sqrt(2)))));
    
y = Cpv(2)*log(To/Tin) - R*log(Po/Pin) + sR1 - sR2;