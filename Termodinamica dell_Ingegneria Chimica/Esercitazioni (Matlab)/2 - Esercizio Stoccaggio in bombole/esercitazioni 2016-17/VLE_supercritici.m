close all
clear all
clc
%dati:
%[H2 N2 NH3 H2O]
%=[1  2  3   4]
global n0 nu Keq_VH p p0 H
nu=[-3/2 -1/2 1 0];
n0=[3 1 0 5]; %mol   ipotizzo tutte in fase v
T=25+273; %K 
p=0.15*1e5; %Pa
p0=[NaN NaN NaN 3.167*1e3]; %Pa
H=[7.158*1e10 9.224*1e10 9.758*1e4 NaN]; %Pa
G0frif=[0 0 -16.33 0]*1e3; %J/mol
R=8.314; %J/molK

G0Rrif=sum(G0frif);
G0R_RT=G0Rrif/(R*T);
Keq_VH=exp(-G0R_RT);

options=optimset('tolfun',1e-10,'maxiter',3e+3,'maxfunevals',10000);
l=fsolve('eqchimico',[5.3442e-08  1.3824e-08  0.46452  0.46452  0.25],options)

