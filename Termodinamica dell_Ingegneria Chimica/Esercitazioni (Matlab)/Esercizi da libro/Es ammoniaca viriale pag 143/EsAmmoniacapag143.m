clc
clear all

format short g
format compact

global Tc Pc om R

%equazione del viriale...

%dati per la sostanza
Tc=405.5; %K
Pc=112.77e5; %Pa
om=0.250; %fattore acentrico di Pitzer

%assegno condizioni di T,P
T1=298;
P1=1e5;
P2=4e5;
%T2=? incognita

R=8.314;              
   
T2=fsolve('Temp',400);
T2

TR1=T1/Tc;
TR2=T2/Tc;

B01=0.083-0.422/TR1^1.6;
B11=0.139-0.172/TR1^1.6;
Buno=(R*Tc/Pc)*(B01+om*B11);
B02=0.083-0.422/TR2^1.6;
B12=0.139-0.172/TR2^1.6;
Bdue=(R*Tc/Pc)*(B02+om*B12);


hrRT1=(P1/(R*T1))*(Buno-T1*((R/Pc)*(0.675/TR1^2.6+om*0.722/TR1^5.2)));
hrRT2=(P2/(R*T2))*(Bdue-T2*((R/Pc)*(0.675/TR2^2.6+om*0.722/TR2^5.2)));
hr1=hrRT1*R*T1;
hr2=hrRT2*R*T2;

I=quad(@(T) 29.747 + (25.108e-3).*T - (1.546e5)./T.^2, T1, T2);

w=I+hr2-hr1



