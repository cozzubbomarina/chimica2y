clear all
clc
format short g

global P Po K F z v

R=8.314;
T=343;
P=0.5; %atm
z=[0.3 0.7 0 0];
F=100;

%A+B-->C+W
v=[-1 -1 1 1];
Dgf=[-37160 -87540 -54070 -71060]*4.186;
Dhf=[-57040 -116200 -121700 -68320]*4.186;

%P torr, T �C
PA=[7.87863 7.18807 7.20211 8.10765];
PB=[-1473.11 -1416.7 -1232.83 -1750.29];
PC=[230 211 228 235];
Po=10.^(PA+PB./(T-273+PC))/760 %atm

Dgo=Dgf*v';
Dg=Dgo+R*T*log(prod(Po.^v));
K=exp(-Dg/(R*T))

Xo=[50 2];
var=fsolve('sistema',Xo);
V=var(1)
L=F-V
lambda=var(2)
x=(F*z+lambda*v)./(V*Po/P+L)
y=Po.*x/P

Xo=[0.1 0.25 0.25 0.25 0.25 0.25 0.25 50 20];
var=fsolve('sistema2',Xo);
V=var(8)
L=F-V
lambda=var(9)
x=[var(1) var(2) var(3) 1-var(1)-var(2)-var(3)]
y=[var(4) var(5) var(6) var(7)]