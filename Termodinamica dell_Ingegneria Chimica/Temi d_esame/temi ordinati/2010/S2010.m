close all
clear all
clc
format short
global  Tc Pc P0B s om R cpl Ti sri Pi F1 z Keq
%% DATI
%1-A, 2-B
R=8.314; %J/(molK)
T=356; %K
P=1e5;  %Pa
Ti=440; %K
Pi=10e5;    %Pa
B1=1000;    %mol/h
A1=2000;    %mol/h
P0B=8e5;     %Pa
cpl=[41.7 39.5];    %J/molK
cpg=[15.3 34];      %J/molK
om=[0.03 0.02];
ant1=[8.071310 8.08097];
ant2=[1730.630 1582.271];
ant3=[233.426 239.726];
Tc=[647 513];
Pc=[220e5 81e5];
%% COMPRESSORE

%% Calcolo della T0B
%entropia residua a Tin e Pin di B
Tri=Ti/Tc(2);
s=[0.37464 1.54226 -0.26992];
S=s(1)+s(2)*om(2)+s(3)*om(2)^2;
k=(1+S*(1-sqrt(Tri)))^2;
a=(0.45724*(R*Tc(2))^2*k)/(Pc(2));
b=(0.07780*R*Tc(2))/(Pc(2));
A=a*Pi/(R*Ti)^2;
B=b*Pi/(R*Ti);
alfa=-1+B;
beta=A-2*B-3*B^2;
gamma=-A*B+B^2+B^3;
coeff=[1 alfa beta gamma];
Z=roots(coeff);
Z=max(Z);
E=S*sqrt(Tri/k);
srR=log(Z-B)-(A*E)/(2*sqrt(2)*B)*log((Z+B*(1+sqrt(2)))/(Z+B*(1-sqrt(2))));
sri=srR*R;
%funzione da azzerare
T0=fsolve('S2010_function1',300);
T_ingresso_B=T0
%calcolo della potenza fornita dal compressore
%entalpia residua a Ti e Pi
Tri=Ti/Tc(2);
s=[0.37464 1.54226 -0.26992];
S=s(1)+s(2)*om(2)+s(3)*om(2)^2;
k=(1+S*(1-sqrt(Tri)))^2;
a=(0.45724*(R*Tc(2))^2*k)/(Pc(2));
b=(0.07780*R*Tc(2))/(Pc(2));
A=a*Pi/(R*Ti)^2;
B=b*Pi/(R*Ti);
alfa=-1+B;
beta=A-2*B-3*B^2;
gamma=-A*B+B^2+B^3;
coeff=[1 alfa beta gamma];
Z=roots(coeff);
Z=max(Z);
E=S*sqrt(Tri/k);
hrRT=Z-1-A/(2*sqrt(2)*B)*(1+E)*log((Z+B*(1+sqrt(2)))/(Z+B*(1-sqrt(2))));
hri=hrRT*R*Ti;
%entalpia residua a T0Be P0B
Tr0=T0/Tc(2);
s=[0.37464 1.54226 -0.26992];
S=s(1)+s(2)*om(2)+s(3)*om(2)^2;
k=(1+S*(1-sqrt(Tr0)))^2;
a=(0.45724*(R*Tc(2))^2*k)/(Pc(2));
b=(0.07780*R*Tc(2))/(Pc(2));
A=a*P0B/(R*T0)^2;
B=b*P0B/(R*T0);
alfa=-1+B;
beta=A-2*B-3*B^2;
gamma=-A*B+B^2+B^3;
coeff=[1 alfa beta gamma];
Z=roots(coeff);
Z=max(Z);
E=S*sqrt(Tr0/k);
hrRT=Z-1-A/(2*sqrt(2)*B)*(1+E)*log((Z+B*(1+sqrt(2)))/(Z+B*(1-sqrt(2))));
hr0=hrRT*R*T0;
Potenza_compressore=B1/3600*(cpl(2)*(Ti-T0)+hri-hr0)

%% REATTORE
% Portata entrante nel flash
F1=A1+B1;
z=[A1/F1 B1/F1];
% Calcolo delle tensioni di vapore
P0=(10.^(ant1-ant2./(T-273.15+ant3)))*133.322;
% Calcolo dei coefficienti di fugacit� a Tflash e Pflash

Tr=T./Tc;
s=[0.37464 1.54226 -0.26992];
S=s(1)+s(2)*om+s(3)*om.^2;
k=(1+S.*sqrt(1-(Tr))).^2;
a=(0.45724*(R*Tc).^2.*k)./Pc;
b=(0.07780*R*Tc)./(Pc);
A=a*P/(R*T)^2;
B=b*P/(R*T);
alfa=-1+B;
beta=A-2*B-3*B.^2;
gamma=-A.*B+B.^2+B.^3;
coeff=[1 alfa(1) beta(1) gamma(1)];
Z1=roots(coeff);
Z(1)=max(Z1);
coeff=[1 alfa(2) beta(2) gamma(2)];
Z2=roots(coeff);
Z(2)=max(Z2);
E=S.*sqrt(Tr./k);
logphi=Z-1-A./(2*sqrt(2)*B).*log((Z+B*(1+sqrt(2)))./(Z+B*(1-sqrt(2))))-log(Z-B);
phi=exp(logphi);
% Calcolo dei coeff di fugacit� a T flash e P0
A=a.*P0./(R*T)^2;
B=b.*P0./(R*T);
alfa=-1+B;
beta=A-2.*B-3*B.^2;
gamma=-A.*B+B.^2+B.^3;
coeff=[1 alfa(1) beta(1) gamma(1)];
Z1=roots(coeff);
Z(1)=max(Z1);
coeff=[1 alfa(2) beta(2) gamma(2)];
Z2=roots(coeff);
Z(2)=max(Z2);
E=S.*sqrt(Tr./k);
logphi0=Z-1-A./(2*sqrt(2)*B).*log((Z+B.*(1+sqrt(2)))./(Z+B*(1-sqrt(2))))-log(Z-B);
phi0=exp(logphi0);

% K di equilibrio fisico
Keq=phi0.*P0./(P*phi);
%V=fsolve('S2010_function2',3000)
syms V
V=solve(sum((F1*z)./((Keq-1)*V+F1).*(1-Keq))==0,V);
V=vpa(V)
L=F1-V
x=(F1*z)./((Keq-1)*V+F1)
y=(F1*z)./((Keq-1)*V+F1).*Keq