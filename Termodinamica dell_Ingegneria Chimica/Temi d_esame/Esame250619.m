clc; clear all; close all;

%% Dati
global F1 F2 m z P0 fiV0 fiV PL
R=8.314; %J/mol/K
TL=323.15+6; %K con delta=6
PL=10; %bar
F1=0.5*1e3; %mol/s
F2=1e3; %mol/s
m=[0 0 1]; %[A B S]
z=[0.45 0.55 0];
C1=[4.537 4.356 4.105];
C2=[-1049 -909 -1626];
C3=[24.9 -2.071 -92.84];
P0=10.^(C1+C2./(TL+C3)); %bar
Tc=[369.9 425.0 558.2]; %K
Pc=[42.5 38.0 28.6]; %bar
ome=[0.153 0.199 0.102];
Tr=TL./Tc;
Pr=PL./Pc;
Pr0=P0./Pc;
B1=0.139-0.172./Tr.^(4.2);
B0=0.083-0.422./Tr.^(1.6);
BPcRTc=B0+ome.*B1;
BPRT=BPcRTc.*(Pr./Tr);
BPRT0=BPcRTc.*(Pr0./Tr);
fiV=exp(BPRT);
fiV0=exp(BPRT0);

%% risoluzione
sol0=[500 700 0.3 0.3 0.4 0.3 0.3 0.4];
options=optimset('MaxFunEvals',30000,'MaxIter',30000,'TolFun',1e-8,'TolX',1e-8);
sol=fsolve(@sistema,sol0,options);
V=sol(1)
L=sol(2)
x=sol(3:5)
y=sol(6:8)

%% cinetica
Vpfr=0.02; %m3
Pre=1e5; %Pa
Tre=373.15; %K
k=1e3; %s^-1 con A->B
v=Pre/(R*Tre);
XA=1-exp(-(k*Pre*Vpfr)/(R*Tre*V))

%% Facoltativa
Tin=298.15; %K
dhev=[18.8 22.4 61.5]*1e3; %J/mol
cp=[80.02 98.61 295.8]; %J/mol/K
cl=[97.32 129.6 376.0]; %J/mol/K
hRV=R*Tin*BPRT-Pr.*(0.675./Tr.^(2.6)+ome.*0.722./Tr.^(5.2));
hRL=R*Tin*BPRT-Pr.*(0.675./Tr.^(2.6)+ome.*0.722./Tr.^(5.2));
hVV=sum(y.*(cp.*(TL-Tin)+hRV));
hLL=sum(x.*(-dhev+cl.*(TL-Tin)+hRL));
hLF=-dhev(3);
hVF=0;
Q=V*hVV+L*hLL-F1*hLF-F2*hVF

%% Sistema
function f=sistema(a)
global F1 F2 m z P0 fiV0 fiV PL
V=a(1); L=a(2); x=a(3:5); y=a(6:8);
f(1)=F1+F2-V-L; %bil mat globale
f(2:4)=F1*m+F2*z-V*y-L*x; %bil specie i
f(5:7)=P0.*fiV0.*x-PL*y.*fiV; %VLE
f(8)=sum(x)-sum(y); %stech
end





