function C=SumMatrix(A,B)

[nA mA]=size(A); %detect the size of matrix A
[nB mB]=size(B);  %detect the size of matrix B



if nA==nB & mA==mB   %logical test to check the compatibility of the two sizes
    for i=1:nA  %cicle that scans the index of the rows
        for j=1:mA   %cicle that scans the index of the columns
            C(i,j)=A(i,j)+B(i,j);   %the i,j element of C is set to be the sum of the corresponding elements of A and B
        end
    end
else   %if the logical test of the if cyclo is false,the function returns an error message
    disp('Dimensions don''t match')
end