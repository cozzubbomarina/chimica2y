clear all
clc
format short g

global tipo Po Tc Pc om T P FA FB phi1 phio1 phi2 phio2
tipo=4;
R=8.314;
P0=8e5;
T1=440; P1=10e5;
T=361; P=1e5;

FA=2000; %mol/h
FB=1000;

T0=fsolve('temp', 400)

Tc1=513; Pc1=81e5; om1=0.02;
tipo=4;
[Z,A,B,S,k]=EoS(T1,P1,Tc1,Pc1,om1,tipo);
E=S*sqrt(T1/(Tc1*k));
[hrRT,srR]=fr(Z(3),A,B,E,tipo);
hr1=hrRT*R*T1;

[Z,A,B,S,k]=EoS(T0,P0,Tc1,Pc1,om1,tipo);
E=S*sqrt(T0/(Tc1*k));
[hrRT,srR]=fr(Z(3),A,B,E,tipo);
hr0=hrRT*R*T0;

w=34*(T1-T0)+hr1-hr0

%pressioni parziali
PA=[8.07131 8.08097];
PB=[1730.63 1582.271];
PC=[233.426 239.726];

T=361; P=1e5;
Po=10.^(PA-PB./(T-273+PC))*1.01325e5/760;
Tc=[647 513];
Pc=[220 81]*10^5;
om=[0.03 0.02];

[Z,A,B,S,k]=EoS(T,P,Tc(1),Pc(1),om(1),tipo);
phi1=phi_puri(Z(3),A,B,tipo)
[Z,A,B,S,k]=EoS(T,Po(1),Tc(1),Pc(1),om(1),tipo);
phio1=phi_puri(Z(3),A,B,tipo)

[Z,A,B,S,k]=EoS(T,P,Tc(2),Pc(2),om(2),tipo);
phi2=phi_puri(Z(3),A,B,tipo)
[Z,A,B,S,k]=EoS(T,Po(2),Tc(2),Pc(2),om(2),tipo);
phio2=phi_puri(Z(3),A,B,tipo)


Xo=[0.5 0.5 1000];
var=fsolve('sistema',Xo);
x(1)=var(1);
x(2)=1-var(1)
y(1)=var(2);
y(2)=1-var(2)
V=var(3)
L=FA+FB-V










