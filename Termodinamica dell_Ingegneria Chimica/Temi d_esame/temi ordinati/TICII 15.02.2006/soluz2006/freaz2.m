%Seconda funzione

function F= freaz2(PT2)

global Keq Pv Q fm0 conv

l1=PT2(1);
l3=PT2(2);
P=PT2(3); %mol

x0=Q.*fm0;

l2=x0(3)*(conv/100);%Conversione 50+x%

m(1)=x0(1)-l1;
m(2)=x0(2)-l1;
m(3)=x0(3)-l2;
m(4)=l1-l2;
m(5)=l2-l3;
x= m./sum(m);

F(1)=(P/Pv) - x(5);
F(2)=(x(4)/(x(1)*x(2)))-Keq(1);
F(3)=(x(5)/(x(3)*x(4)))-Keq(2);