%Pv calcolo della tensione di vapore usando EoS cubica

clc
clear all
format compact

global T Tc Pc om tipo

%dati per n-ottano
Tc=569.4; %K
Pc=24.97e5; %Pa
om=0.398; %fattore acentrico di Pitzer
Tn=398.9; %Teb normale

tipo=3; %viene scelta l'Eos da usare (RKS in questo caso)
R=8.314;
T=427.9;

%stima del valore di P� con approssimazione della Clapeyron basata su Tc e
%Tn
BCl=(log(Pc/1.01325e5))/(1/Tn-1/Tc);    %costante B
ACl=(log(1.01325e5))+BCl/Tn;            %costante A
PCl=exp(ACl-BCl/T);             %stima di P� con Clapeyron
P0=fsolve('phi',PCl)            %calcolo P�