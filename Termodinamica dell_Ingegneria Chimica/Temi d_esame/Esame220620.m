clc; clear all; close all;

%con EoS del viriale phi(i)=exp(P*B(T)/R/T) con B*Pc/R/Tc=B0(Tr)+B1(Tr)*om
%in cui Pr=P/Pc e Tr=T/Tc

global Keq P0 P F z nu phiV zV v Tc T P R om Pc ni0 nu k ro V zC G
TF=298; %K
T=353.15;
P=0.5; %bar
F=80; %kmol/h
R=8.314; %J/mol/K
z=[0.2 0.15 0 0.65]; %[A B C S]
nu=[-2 -1 1 0];
A=[2.69607 4.22368 2.4561 5.0768];
B=[621.275 1302.256 1001.213 1659.753];
C=[-121.929 -71.788 -55.162 -45.854];
P0=10.^(A-B./(C+T)); %bar
dg0f=[-26.99 -115.77 -177.35 -145.43]*1e3;
Tc=[616.57 613.1 824.1 647.2]; %K
Pc=[52.43 67.07 92.31 220.64]; %bar
om=[0.9001 0.8318 0.8019 0.8301];
Keq=exp(-(sum(nu.*dg0f))/R/T); 
Tr=T./Tc;
Pr=P./Pc;
B0=0.083-0.422./Tr.^(1.6);
B1=0.139-0.172./Tr.^(4.2);
BPRT=(B0+B1.*om).*Tr./Pr;
phiV=exp(BPRT);
zV=1+BPRT;
v=zV.*R.*T./P;
ni0=F*z;
k=0.005; %m6/kmol2/h
ro=55; %kmol/m3
V=1; %m3

%soluzione parte A
sol0=[0.1 0.1 0.1 0.7 0.1 0.1 0.1 0.7 40 40 8];
options=optimset('MaxFunEvals',30000,'TolX',1e-8,'TolFun',1e-8);
sol=fsolve(@sistema,sol0,options);
L=sol(9)
V=sol(10)
y=sol(1:4)
x=sol(5:8)


%soluzione parte B
solC0=[0.1 0.1 0.1 0.7 80];
solC=fsolve(@CSTR,solC0,options)
zC=solC(1:4);
G=solC(5);

% flash VLE
solV0=[40 40 0.1 0.1 0.1 0.7 0.1 0.1 0.1 0.7];
solV=fsolve(@flash,solV0,options);
L=solV(2);

LCVLE=L*solV(9)
LCC=sol(9)*sol(7)

deltaL=LCVLE-LCC


function f=flash(m)
global zC G P phiV P0 v zV Tc Pc om R T z
V=m(1); L=m(2); y=m(3:6); x=m(7:10);

for i=1:4
    for j=1:4
        vcm(i,j)=((v(i)^(1/3)+v(j)^(1/3))/2)^3;
        Zcm(i,j)=mean([zV(i), zV(j)]);
        %kc(i,j)=1-sqrt(v(i)*v(j))/vcm(i,j);
        Tcm(i,j)=sqrt(Tc(i)*Tc(j));
        omm(i,j)=mean([om(i), om(j)]);
    end
end
Pcm=Zcm.*R.*Tcm./vcm;
TRm=T./Tcm;
PRm=P./Pcm;

B0ij=0.083-0.422./TRm.^1.6;
B1ij=0.139-0.172./TRm.^4.2;
BijPRT=(B0ij+omm.*B1ij).*PRm./TRm;
BPRT=y*BijPRT*y';
logphi=-BPRT+2*y*BijPRT;
phi=exp(logphi);

f(1)=G-V-L;
f(2:5)=G*zC-V*y-L*z;
f(6:9)=P.*y.*phi-P0.*phiV;
f(10)=sum(y)-1;
end


function f=CSTR(m)
global ni0 nu k ro V
x=m(1:4); nout=m(5);
ni=x*nout;

f(1:4)=ni0-ni+nu.*k*(x(1)*ro)^2.*(x(2)*ro)*V;
f(5)=sum(x)-1;

end

function f=sistema(m)
global Keq P0 P F z nu phiV zV v Tc T P R om Pc
y=m(1:4); x=m(5:8); L=m(9); V=m(10); lam=m(11); %con 0<lam<12


for i=1:4
    for j=1:4
        vcm(i,j)=((v(i)^(1/3)+v(j)^(1/3))/2)^3;
        Zcm(i,j)=mean([zV(i), zV(j)]);
        %kc(i,j)=1-sqrt(v(i)*v(j))/vcm(i,j);
        Tcm(i,j)=sqrt(Tc(i)*Tc(j));
        omm(i,j)=mean([om(i), om(j)]);
        PRm(i,j)=mean([Pc(i), Pc(j)]);
    end
end
Pcm=Zcm.*R.*Tcm./vcm;
TRm=T./Tcm;
PRm=P./Pcm;

B0ij=0.083-0.422./TRm.^1.6;
B1ij=0.139-0.172./TRm.^4.2;
BijPRT=(B0ij+omm.*B1ij).*PRm./TRm;
BPRT=y*BijPRT*y';
logphi=-BPRT+2*y*BijPRT;
phi=exp(logphi);


f(1)=Keq-(y(3)*phi(3)/(y(2)*phi(2)*(P*y(1)*phi(1))^2));
f(2:5)=P0.*x.*phiV-P*y.*phi;
f(6:9)=F*z-L*x-V*y+nu*lam;
f(10)=F-L-V+sum(nu.*lam);
f(11)=sum(x)-1;
end






