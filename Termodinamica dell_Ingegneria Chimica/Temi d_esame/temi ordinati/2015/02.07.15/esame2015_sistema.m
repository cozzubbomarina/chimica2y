function f=eqsame2015_sistema(X)
global n0 nu G0RrifRT w 
%definisco le incognite del sistema:
l=X(1);
T1=X(2);

%eq chimico:
I=45.28786804868936631446339847011 - 6.2281477026701948520567717103681*log(T1) - 2381.0267823765335578542217945634/T1 - 0.0060922059177291315852778445994708*T1;
G0RRT=G0RrifRT-I; %funzione di T1, incognita
%oppure: argexp=45.28786804868936631446339847011 - 6.2281477026701948520567717103681*log(T1) - 2381.0267823765335578542217945634/T1 - 0.0060922059177291315852778445994708*T1
%Keq_VH=Keq_VHrif*exp(-argexp); 
Keq_VH=exp(-G0RRT);

n=n0+l*nu;
ntot=sum(n);
w=n/ntot; %vettore frazioni molari incognite
att=w;
Keq_att=prod((att).^nu);

%per bilancio energia:
H0RdiT1=- 0.0000010465*(2420.0*T1 + 3195523.0)*(20.0*T1 - 5963.0) - 1451/10;
S=0.0010465*(1029.0*T1 + 3065200.0)*(T1 - 600.0);

f(1)=Keq_att-Keq_VH; %eq chimico funz di l e T1
f(2)=S-l*H0RdiT1;%bilancio di energia