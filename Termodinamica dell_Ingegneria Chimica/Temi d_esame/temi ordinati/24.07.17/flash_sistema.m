function f=flash_sistema(x)
global F z p p0
xA=x(1);
xB=x(2);
yA=x(3);
yB=x(4);
V=x(5);
L=x(6);

A=1.2; %A12=A
B=1.4; %A21=B
gammaA=exp((xB^2)*(A+2*(B-A)*xA));
gammaB=exp((xA^2)*(A+2*(B-A)*xB));

%sistema:
f(1)=F-V-L; %BMG
f(2)=F*z(1)-V*yA-L*xA; %BMSS A
f(3)=F*z(2)-V*yB-L*xB; %BMSS B
f(4)=p*yA-p0(1)*xA*gammaA;%VLE A
f(5)=p*yB-p0(2)*xB*gammaB;%VLE B
f(6)=xA+xB-1;%STECH


