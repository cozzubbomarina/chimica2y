format short

%dati dell'ossigeno
Tc=154.6;
pc=50.43e5;
om=0.022;

%variabili assegnate dal problema
T=298;
p=100e5;

%costanti
R=8.314;
RT=R*T;
Tr=T/Tc;
pr=p/pc;

%RK
a=(0.42748*R^2*Tc^2.5)/pc;
b=(0.08664*R*Tc)/pc;
A=(a*p)/RT^2;
B=(p*b)/RT;
alfa=-1;
beta=A-B-B^2;
gamma=-A*B;
coeff=[1 alfa beta gamma]
z=roots(coeff);
z=sort(z)

v=(z*RT)/p
