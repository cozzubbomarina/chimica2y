function f=Temaesame2015_f4(x)
global nu k S Vol z
Q=x(1);
nb=x(2);
ni=x(3);
np=x(4);
no=x(5);
na=S*0.05/(1+Vol*k*no*0.05/(Q^2));
n=[na nb ni np no];
R=k*na*no/(Q^2);
f(1:5)=S*z-n+Vol*nu*R;