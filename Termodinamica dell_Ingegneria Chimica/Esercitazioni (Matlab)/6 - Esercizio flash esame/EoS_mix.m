function [Z,A,B,Ap,Bp,E]=EoS_mix(T,P,x,Tc,Pc,om,tipo)
%EoS_mix calcola Z con diverse Eos
%dati: T,P,x(vettore),Tc(vettore),Pc(vettore),om(vettore),tipo
%                                                       1 VdW
%                                                       2 RK
%                                                       3 RKS    
%                                                       4 PR
%restituisce Z,A,B,E della miscela e Ap,Bp dei composti puri

R=8.314;
TR=T./Tc;

%parametri dell'equazione nella forma Z^3+alfa*Z^2+beta*Z+gamma=0
if tipo==1      %vdW
    S=0;    
    k=1;    
    a=27*(R*Tc).^2./(64*Pc);   
    b=R*Tc./(8*Pc); 
    Ap=a.*P./(R*T)^2;
    Bp=b.*P./(R*T);
    
    am=(sum(x.*sqrt(a)))^2;
    bm=sum(x.*b);    
    A=am*P/(R*T)^2;
    B=bm*P/(R*T);
    alfa=-1-B;
    beta=A;
    gamma=-A*B;
end
if tipo==2      %RK
    S=0;    
    k=1;
    a=0.42748*(R*Tc).^2./(Pc.*sqrt(TR));   
    b=0.08664*R*Tc./Pc;          
    Ap=a.*P./(R*T)^2;
    Bp=b.*P./(R*T);
    
    am=(sum(x.*sqrt(a)))^2;
    bm=sum(x.*b);
    A=am*P/(R*T)^2;
    B=bm*P/(R*T);
    alfa=-1;
    beta=A-B-B^2;
    gamma=-A*B;    
end
if tipo==3      %RKS
    S=0.48+1.574*om-0.176*om.^2;
    k=(1+S.*(1-sqrt(TR))).^2;
    a=0.42748*k.*(R*Tc).^2./Pc;
    b=0.08664*R*Tc./Pc;
    Ap=a.*P./(R*T)^2;
    Bp=b.*P./(R*T);
    
    am=(sum(x.*sqrt(a)))^2;
    bm=sum(x.*b);
    A=am*P/(R*T)^2;
    B=bm*P/(R*T);
    alfa=-1;
    beta=A-B-B^2;
    gamma=-A*B;
end
if tipo==4      %PR
    S=0.37464+1.54226*om-0.26992*om.^2;
    k=(1+S.*(1-sqrt(TR))).^2;
    a=0.45724*k.*(R*Tc).^2./Pc;
    b=0.07780*R*Tc./Pc;
    Ap=a.*P./(R*T)^2;
    Bp=b.*P./(R*T);
    
    am=(sum(x.*sqrt(a)))^2;
    bm=sum(x.*b);
    A=am*P/(R*T)^2;
    B=bm*P/(R*T);
    alfa=-1+B;
    beta=A-2*B-3*B^2;
    gamma=-A*B+B^2+B^3;
end

%calcolo parametro per funzioni reasidue
E=sum(x.*S.*sqrt(a.*TR./k))./sqrt(am);

%risoluzione numerica cubica
coeff=[1 alfa beta gamma];
Z=roots(coeff);     %calcola le radici del polinomio
Z=sort(Z);          %ordina i valori