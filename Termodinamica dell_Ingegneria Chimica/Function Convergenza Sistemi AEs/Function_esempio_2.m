%fmain_neg  Esempio di equazioni risolte con fsolve_HC
%           x1^2-1=0; x2^2-0.25=0; senza vincoli

% R. Rota (2020)

function F=fmain_neg(x)

f(1) = x(1)^2-1;
f(2) = x(2)^2-0.25;

F  = f;