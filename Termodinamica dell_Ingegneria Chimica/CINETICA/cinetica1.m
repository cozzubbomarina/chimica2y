% main esercizio 1
% R. Rota 2013
clear all
close all
global k v
% dati comuni
v = [ -1 1 0 0 0 %matrice stechiometrica
0 -1 -1 1 1]';
k = [0.2 10]; %costanti cinetiche [kmol - m3 - s]
y0 = [1 0 1 0 0]'; %concentrazioni iniziali/ingresso [kmol/m3]
tau= 5*60; %tempo di residenza totale [s]
% PFR/Batch
[t,y] = ode23(@fBatch_1,[0 tau],y0); %integrazione ODEs
yF = y(length(t),:) %scritta a video
%yR= y(tau,:) %nooo perch� ha solo 76 righe!