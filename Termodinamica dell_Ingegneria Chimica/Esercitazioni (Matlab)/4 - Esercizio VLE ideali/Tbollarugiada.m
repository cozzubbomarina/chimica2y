clear all
clc

global P A B C Tc Pc om x

%butano (1)
A(1)=6.83039;
B(1)=-945.9;
C(1)=240;
Tc(1)=425.5;
Pc(1)=37.5; %atm
om(1)=0.201;

%pentano (2)
A(2)=6.85221;
B(2)=-1064.63;
C(2)=232;
Tc(2)=469.5;
Pc(2)=33.3; %atm
om(2)=0.252;

%composizione, condizioni
x(2)=0.25;
x(1)=1-x(2);
P=15; %atm

Tb=fsolve('tempb',200);      %T bolla
Td=fsolve('tempd',200);     %T rugiada
disp('Gas perfetto');
Tb
Td

Tb=fsolve('tempbvir',100);      %T bolla 
Td=fsolve('tempdvir',100);      %T rugiada 
disp('Viriale');
Tb
Td