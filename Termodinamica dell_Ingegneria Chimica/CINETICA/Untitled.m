%es cinetica 3 BATCH
%[A B C]
clc 
clear all
global k nu
y0=[5 0 0]'; %mol/m3
nu=[-2 1 0
 0 -1 1]';
k=[1 1e3]./3600; %m3/s mol
tau=3600; %s

[t,y]=ode23('batch_es3',[0 tau],y0);
yF = y(length(t),:)

