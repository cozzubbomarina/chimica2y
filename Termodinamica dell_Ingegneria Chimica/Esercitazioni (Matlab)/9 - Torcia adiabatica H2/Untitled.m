clear all
clc
format short g

global P v no Ks a b c ao bo co Dho To

P=1.01325; %bar
To=298;
R=8.314;

% 1/2 O2+ H2-->H2O   +N2 inerte
v=[-1/2 -1 1 0];
no=[0.5 1 0 2];

Dhf=[0 0 -57800 0]*4.186;
Dgf=[0 0 -54640 0]*4.186;

cpa=[6.148 6.524 7.256 6.830]*4.186;
cpb=[0.003102 0.00125 0.0023 0.0009]*4.186;
cpc=[0 0 0 12000]*4.186;
a=cpa*v';
b=cpb*v';
c=cpc*v';
ao=cpa*no';
bo=cpb*no';
co=cpc*no';

Dho=Dhf*v';
Dgo=Dgf*v';
Ko=exp(-Dgo/(R*298))

syms t1 t2 T Ks
Ks=Ko*exp( int( (Dho+int(a+b*t1+c*t1^-2, t1,298,t2))/(R*t2^2) ,t2,298,T));

Xo=[2000 0.5];
var=fsolve('sistema',Xo);
T=var(1)
lambda=var(2)

K=eval(Ks)
n=no+lambda*v
x=n/sum(n)





























