clc; clear all; close all;

global k rho V n0 nu dg0R R Tr gammaB gammaA F G zCSTR b dhfus Tf T zId
delta=0;
V=2; %m3
F=100; %kmol/h
xa=0.1+delta/30;
rho=50; %kmol/m3
k=0.1; %m3/kmol/h
Tr=350; %K
P=1; %bar
zin=[xa 0 1-xa];
n0=F*zin;
nu=[-2 1 0];
dg0f=[13.5 13.7 10.04]*1e3; %J/mol
dg0R=sum(nu.*dg0f);
gammaA=0.7; gammaB=1.3; gammaS=0.5;
R=8.314; %J/mol/K
T=220; %K
dhfus=[28.57 35 13.61]*1e3; %J/mol
Tf=[255.3 297 208]; %K
cl=[150.46 132 158.11]; %J/mol/K
csB=132; 

% CSTR
sol0=[35 31 34 99];
sol=fsolve(@CSTR,sol0);
nCSTR=sol(1:3); %kmol/h
G=sol(4);
zCSTR=nCSTR/G

% Stadio ideale
lam=fsolve(@ideale,4);
xa=(10-2*lam)/(F-lam); xb=lam/(F-lam); xs=90/(F-lam);
zId=[xa xb xs]

%cristallizzatore CSTR
b=[0 1 0];
solC=[50 50 0.3 0.3 0.3];
crisC=fsolve(@cristallizzatoreCSTR,solC)
BC=crisC(2);

%cristallizzatore Ideale
crisI=fsolve(@cristallizzatoreideale,solC)
BI=crisI(2);

DeltaB=BI-BC

% Bilancio energia
Q=(BC*1000/3600*(cl(2)*(Tf(2)-T)-dhfus(2)+csB*(Tr-Tf(2)))-G*1000/3600*(sum(zCSTR.*cl*(Tr-T))))/1000 %kW


function f=CSTR(b)
ni=b(1:3); n=b(4);
global k rho V n0 nu

f(1:3)=n0-ni+nu.*k*V*(rho*ni(1)/n)^2;
f(4)=ni(1)/n+ni(2)/n+ni(3)/n-1;
end

function f=ideale(lam)
global dg0R R Tr gammaB gammaA F
xa=(10-2*lam)/(F-lam); xb=lam/(F-lam);

f=(xb*gammaB)/(xa*gammaA)^2-exp(-dg0R/(R*Tr));
end

function f=cristallizzatoreCSTR(j)
global G zCSTR b dhfus R Tf T gammaB
M=j(1); B=j(2); x=j(3:5);

f(1)=G-M-B;
f(2:4)=G*zCSTR-M*x-B*b;
f(5)=x(2)*gammaB-exp(dhfus(2)/R*(1/Tf(2)-1/T));
end

function f=cristallizzatoreideale(j)
global F zId b dhfus R Tf T gammaB
M=j(1); B=j(2); x=j(3:5);

f(1)=F-M-B;
f(2:4)=F*zId-M*x-B*b;
f(5)=x(2)-exp(dhfus(2)/R*(1/Tf(2)-1/T))/gammaB;
end


