function y=sistema_mix_id_gas_id(l)

global k_fis_all_id F Aria yaria xF 

L=l(1);
V=l(2);
for i=1:4
    xL(i)=l(i+2);
    yV(i)=l(i+6);
end

%--------------------------------------------------------------------------
% Gamma(x)
%--------------------------------------------------------------------------

gamma(1)=exp(0.5.*xL(2).^2);
gamma(2)=exp(0.5.*xL(1).^2);

correzione=[gamma(1) gamma(2) 1 1 ];

k_fis=k_fis_all_id.*correzione;

%--------------------------------------------------------------------------
       
y(1:4)=F.*xF+Aria.*yaria-V.*yV-L.*xL; % 4 BMC
y(5:8)=yV-k_fis.*xL;                  % 4 Eq. L/V
y(9)=sum(yV)-1;                       % Stech. Vap
y(10)=sum(xL)-1;                      % Stech. Liq


