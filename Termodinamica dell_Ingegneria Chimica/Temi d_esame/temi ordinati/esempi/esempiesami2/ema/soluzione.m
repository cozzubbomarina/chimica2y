function f=soluzione(X)

global DG1 DG2 no nu

T=110+273.15; %K
R=8.314472;
G=1.5;

% Calcolo delle frazioni molari: 
% a:fase alfa b:fase beta
n=no+X*nu;
na=n(1:4);
nb=n(5:8);
nta=sum(na);
ntb=sum(nb);
xa=na/nta; % XAa=xa(1) ; XBa=xa(2) ; XCa=xa(3) ; XS1a=xa(4)
xb=nb/ntb; % XBb=xb(1) ; XCb=xb(2) ; XPb=xb(3) ; XS2b=xb(4)

% Calcolo gamma:
gammaBa=exp(G*xa(3)^2);
gammaBb=exp(G*xb(2)^2);
gammaCa=exp(G*xa(2)^2);
gammaCb=exp(G*xb(1)^2);

% Coeff reaz 1:
attBa=xa(2)*gammaBa;
attCa=xa(3)*gammaCa;
attAa=xa(1);

% Coeff reaz 2:
attPb=xb(3);
attBb=xb(1)*gammaBb;
attCb=xb(2)*gammaCb;

%equazioni:
f(1)=attBa*attCa/attAa^2-exp(-DG1/T/R);
f(2)=attPb/attBb/attCb-exp(-DG2/T/R);
f(3)=xa(2)*gammaBa-xb(1)*gammaBb;
f(4)=xa(3)*gammaCa-xb(2)*gammaCb;



