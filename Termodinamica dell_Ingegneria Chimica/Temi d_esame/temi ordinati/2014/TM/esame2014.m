%esame 2014
%TM 6/2017
clear all
close all
clc

global ni n0 P H k2

%%dati s=[O2v N2v CH3CHO CH3COOH O2l N2l] = [1 2 3 4 5 6]

F1=200;     %mol/s
F2=100;     %mol/s
z1=[.21 .79 0 0 0 0];   %composizione corrente in fase vapore
z2=[0 0 1 0 0 0];       %composizione corrente liquida
Ts=470;     %K
P=5;        %bar
aH=[-658 3351];
bH=[16.084 2.884];
cH=[-0.0269 -0.0163];
dgof=[0 0 -196.4 -207.2 0 0]*1000;
dhof=[0 0 -229.4 -232.2 0 0]*1000;
R=8.314;

n0=[F1*z1(1) F1*z1(2) F2*z2(3) 0 0 0];
ni=[0 0 -1 1 -.5 0 %coeff stech fase l
    -1 0 0 0 1 0 %coeff stech fase v O2
    0 -1 0 0 0 1]'; %coeff stech fase v N2
ni1=ni(:,1)'
ni2=ni(:,2)';
ni3=ni(:,3)';

H=aH+bH*Ts+cH*Ts^2;

dG0r1=sum(dgof.*ni1);
krif1=exp(-dG0r1/(298*R));

dH0r1=sum(dhof.*ni1);

syms T

k2=krif1*exp( int(dH0r1/(R*T^2),T,298,Ts) );
k2=eval(k2);

l=fsolve('e2014sistema', [1 1 1])'
clc
n=n0'+ni*l;
nv=[n(1) n(2)];
nl=[n(3) n(4) n(5) n(6)];
ntl=sum(nl);
ntv=sum(nv);
x=[0 0 nl/ntl];
y=[nv/ntv 0 0 0 0];

disp(['composizione fase liquida: ',num2str(x)]);
disp(['composizione fase vapore: ',num2str(y)]);
