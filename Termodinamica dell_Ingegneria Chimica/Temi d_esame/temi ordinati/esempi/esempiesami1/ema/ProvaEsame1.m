clc 
clear all 
format short g

global P ya yb za zb F1 T no nu DGor

%DATI:
P=2.5; %bar
F1=10; %kmol/h
za=0.6;
zb=1-za;
ya=0.7;
yb=1-ya;
F2=2; %kmol/h
DGor=-12*1000; %J/mol

%soluzione flash: 

xo=[0.5 0.5 5 5 100];
f=fsolve('sist',xo);

xa=f(1)
xb=f(2)
V=f(3)
L=f(4)
T=f(5)+273.15 %K

%soluzione reattore:

%S=[A B C D]
no=[V*ya V*yb F2 0];
nu=[-2 0 -1 1];

xoo=0.5;
g=fsolve('sist1',xoo);

lambda=g

n=no+nu*lambda;
ntot=sum(n);

xa=n(1)/ntot
xb=n(2)/ntot
xc=n(3)/ntot
xd=n(4)/ntot

G=V+F2-2*lambda