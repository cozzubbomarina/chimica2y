function f=equaz1(alfa)
global z y
f=((z(1)-alfa*y(1))/(1-alfa))+((z(2)-alfa*y(2))/(1-alfa))-1;

%scrivo nel main:%trovo alfa azzerando la prima equaz
%alfa=fzero('equaz1',0.5);