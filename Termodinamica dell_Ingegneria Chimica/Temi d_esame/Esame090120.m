clc; clear all; close all;

global G H z m Tl alfaAB alfaAS
delta=0;
F=1000/3600; %kmol/s
k=1.5; %kmol/m3/s
Tr=328.15; %K
Pr=1.01325; %bar
XA=(90+delta)/100; %
zA=1-XA;
zB=XA;
G=1000; %kmol/h
Vr=F*zB/(k*zA) %m3
z=[zA zB 0];
H=500; %kmol/h
Tl=298.15; %K
m=[0 0 1];
alfaAB=600; %K
alfaAS=700; %K
cp=[200 190 140]; %J/mol/K

% soluzione
sol0=[500 500 0.05 0.5 0.05 0.5]; %attento ai valori di primo tentativo di xa1 e xa2
options=optimset('MaxFunEvals',50000,'MaxIter',50000,'TolX',1e-8,'TolFun',1e-8);
sol=fsolve(@sistema,sol0,options);
L1=sol(1) %kmol/h
L2=sol(2) 
x1=[sol(3) sol(4) 0]
x2=[sol(5) 0 sol(6)]

Q=G*(-sum(z.*cp*(Tr-Tl)))/3600 %kW (perch� G � in kmol e non in mol)

function f=sistema(c)
global G H z Tl alfaAS alfaAB
L1=c(1); L2=c(2); x1=[c(3) c(4) 0]; x2=[c(5) 0 c(6)];
gamma1=exp(alfaAB/Tl*(1-x1(1))^2);
gamma2=exp(alfaAS/Tl*(1-x2(1))^2);

f(1)=G+H-L1-L2;
f(2)=G*z(1)-L1*x1(1)-L2*x2(1);
f(3)=G*z(2)-L1*x1(2);
f(4)=H-L2*x2(3);
f(5)=sum(x1-x2);
f(6)=x1(1)/x2(1)-gamma2/gamma1;
end

%sistema semplificato a 4eqs in 4inc

% function f=sistema(c)
% global G H z m Tl alfaAS alfaAB
% L1=c(1); L2=c(2); x1a=c(3); x2a=c(4);
% gamma1=exp(alfaAB/Tl*(1-x1a)^2);
% gamma2=exp(alfaAS/Tl*(1-x2a)^2);
% 
% f(1)=G*z(1)-L1*x1a-L2*x2a;
% f(2)=G*z(2)-L1*(1-x1a);
% f(3)=H-L2*(1-x2a);
% f(4)=x1a/x2a-gamma2/gamma1;
% end






