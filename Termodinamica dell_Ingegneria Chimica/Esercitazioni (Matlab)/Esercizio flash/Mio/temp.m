function f=temp(T0)

cp=34;
R=8.314;

P0=8e5;
T1=440; P1=10e5;
Tc=513; Pc=81e5; om=0.02;
tipo=4;

[Z,A,B,S,k]=EoS(T1,P1,Tc,Pc,om,tipo);
E=S*sqrt(T1/(Tc*k));
[hrRT,srR]=fr(Z(3),A,B,E,tipo);
sr1=srR*R;

[Z,A,B,S,k]=EoS(T0,P0,Tc,Pc,om,tipo);
E=S*sqrt(T0/(Tc*k));
[hrRT,srR]=fr(Z(3),A,B,E,tipo);
sr0=srR*R;

f=cp*log(T1/T0)-R*log(P1/P0)+sr1-sr0;