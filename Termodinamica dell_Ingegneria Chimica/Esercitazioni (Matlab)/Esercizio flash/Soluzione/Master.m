clear all
clc

global Tin Pin Po om R Tc Pc sR2 Cpv T P PevA PevB F zA phiA phiB phi0A phi0B

%******************************* PUNTO 1 ****************************
%DATI
Tin = 440;   %K
Po  = 8;     %bar
Pin = 10;     %bar
FA = 2000; %mol/h
FB = 1000; %mol/h
R   = 8.314; %J/mol/K
%dati flash
T = 365;   %K
P  = 1;     %bar

%        A              B
Tc   = [647            513]; %K
Pc   = [220             81]; %bar
om   = [0.03          0.02];
antA = [8.071310   8.08097];
antB = [1730.630  1582.271];
antC = [233.426    239.726];
Cpv  = [15.3          34.0]; %J/mol/K

%controllo esistenza fase gassosa corrente B a valle
Pev_in = (10^(antA(2) - antB(2)/((Tin-273.15) + antC(2))))/750 %bar
Pin

%Soluzione approssimata: gas perfetto
To_a = Tin * (Po/Pin)^(R/Cpv(2))
Ws_a = Cpv(2) * (Tin - To_a) 

%Soluzione reale
    %calcolo hR2 e sR2 (Tin)
    S = 0.37464+1.54226*om(2)-0.26992*om(2)^2;
    k = (1+S*(1-(Tin/Tc(2))^0.5))^2;
    a = 0.45724*k*R^2*Tc(2)^2 / (Pc(2)*100000);
    b = 0.07780*R*Tc(2) / (Pc(2)*100000);
    A = a*Pin*100000/(R*Tin)^2;
    B = b*Pin*100000/(R*Tin);
    E = S*(Tin/Tc(2)/k)^0.5;
    alfa = -1+B;
    beta = A-2*B-3*B^2;
    gamma = -A*B+B^2+B^3;
    coeff =[ 1
         alfa
         beta 
         gamma];
    Z = roots(coeff); %calcola le radici del polinomio
    Z = sort(Z);      %ordina in modo crescente le soluzioni
    ZV = Z(3);
    hR2 = R*Tin*(ZV-1-A/(2*sqrt(2)*B)*(1+E)*log((ZV+B*(1+sqrt(2)))/(ZV+B*(1-sqrt(2)))))
    sR2 = R*(log(ZV-B)-A*E/(2*sqrt(2)*B)*log((ZV+B*(1+sqrt(2)))/(ZV+B*(1-sqrt(2)))))

options=optimset('Display','Iter','Tolfun',1e-10,'MaxIter',3e+3,'MaxFunEvals',10000);
To=fsolve(@fesame1,To_a,options)

%calcolo hR1 e sR1 (To)
    k = (1+S*(1-(To/Tc(2))^0.5))^2;
    a = 0.45724*k*R^2*Tc(2)^2 / (Pc(2)*100000);
    b = 0.07780*R*Tc(2) / (Pc(2)*100000);
    A = a*Po*100000/(R*To)^2;
    B = b*Po*100000/(R*To);
    E = S*(To/Tc(2)/k)^0.5;
    alfa = -1+B;
    beta = A-2*B-3*B^2;
    gamma = -A*B+B^2+B^3;
    coeff =[ 1
         alfa
         beta 
         gamma];
    Z = roots(coeff); %calcola le radici del polinomio
    Z = sort(Z);      %ordina in modo crescente le soluzioni
    ZV = Z(3);
    hR1 = R*To*(ZV-1-A/(2*sqrt(2)*B)*(1+E)*log((ZV+B*(1+sqrt(2)))/(ZV+B*(1-sqrt(2)))))
    sR1 = R*(log(ZV-B)-A*E/(2*sqrt(2)*B)*log((ZV+B*(1+sqrt(2)))/(ZV+B*(1-sqrt(2)))))

Ws = Cpv(2)*(Tin - To) + hR2 - hR1

%controllo esistenza fase gassosa corrente B a monte
Pev_o = (10^(antA(2) - antB(2)/((To-273.15) + antC(2))))/750 %bar
Po

%controllo esistenza fase liquida corrente A
Pev_A = (10^(antA(1) - antB(1)/((Tin-273.15) + antC(1))))/750 %bar
Pin


%******************************* PUNTO 2 ****************************
%risoluzione approssimata (utilizzata come primo tentativo)
F = FA + FB;
zA = FA/F;
zB = 1 - zA;

PevA = (10^(antA(1) - antB(1)/((T-273.15) + antC(1))))/750; %bar
PevB = (10^(antA(2) - antB(2)/((T-273.15) + antC(2))))/750; %bar

%calcolo phiA,v(T,P)    A=1
    S = 0.37464+1.54226*om(1)-0.26992*om(1)^2;
    k = (1+S*(1-(T/Tc(1))^0.5))^2;
    a = 0.45724*k*R^2*Tc(1)^2 / (Pc(1)*100000);
    b = 0.07780*R*Tc(1) / (Pc(1)*100000);
    A = a*P*100000/(R*T)^2;
    B = b*P*100000/(R*T);
    E = S*(T/Tc(1)/k)^0.5;
    alfa = -1+B;
    beta = A-2*B-3*B^2;
    gamma = -A*B+B^2+B^3;
    coeff =[ 1
         alfa
         beta 
         gamma];
    Z = roots(coeff); %calcola le radici del polinomio
    Z = sort(Z);      %ordina in modo crescente le soluzioni
    ZV = Z(3);
    logphiV = ZV-1-A/(2*sqrt(2)*B)*log((ZV+B*(1+sqrt(2)))/(ZV+B*(1-sqrt(2))))-log(ZV-B);
    phiA = exp(logphiV);
    
%calcolo phiB,v(T,P)    B=2
    S = 0.37464+1.54226*om(2)-0.26992*om(2)^2;
    k = (1+S*(1-(T/Tc(2))^0.5))^2;
    a = 0.45724*k*R^2*Tc(2)^2 / (Pc(2)*100000);
    b = 0.07780*R*Tc(2) / (Pc(2)*100000);
    A = a*P*100000/(R*T)^2;
    B = b*P*100000/(R*T);
    E = S*(T/Tc(2)/k)^0.5;
    alfa = -1+B;
    beta = A-2*B-3*B^2;
    gamma = -A*B+B^2+B^3;
    coeff =[ 1
         alfa
         beta 
         gamma];
    Z = roots(coeff); %calcola le radici del polinomio
    Z = sort(Z);      %ordina in modo crescente le soluzioni
    ZV = Z(3);
    logphiV = ZV-1-A/(2*sqrt(2)*B)*log((ZV+B*(1+sqrt(2)))/(ZV+B*(1-sqrt(2))))-log(ZV-B);
    phiB = exp(logphiV);
    
%calcolo phi0A,v(T,PevA(T))    A=1
    S = 0.37464+1.54226*om(1)-0.26992*om(1)^2;
    k = (1+S*(1-(T/Tc(1))^0.5))^2;
    a = 0.45724*k*R^2*Tc(1)^2 / (Pc(1)*100000);
    b = 0.07780*R*Tc(1) / (Pc(1)*100000);
    A = a*PevA*100000/(R*T)^2;
    B = b*PevA*100000/(R*T);
    E = S*(T/Tc(1)/k)^0.5;
    alfa = -1+B;
    beta = A-2*B-3*B^2;
    gamma = -A*B+B^2+B^3;
    coeff =[ 1
         alfa
         beta 
         gamma];
    Z = roots(coeff); %calcola le radici del polinomio
    Z = sort(Z);      %ordina in modo crescente le soluzioni
    ZV = Z(3);
    logphiV = ZV-1-A/(2*sqrt(2)*B)*log((ZV+B*(1+sqrt(2)))/(ZV+B*(1-sqrt(2))))-log(ZV-B);
    phi0A = exp(logphiV);
    
%calcolo phi0B,v(T,PevB(T))    B=2
    S = 0.37464+1.54226*om(2)-0.26992*om(2)^2;
    k = (1+S*(1-(T/Tc(2))^0.5))^2;
    a = 0.45724*k*R^2*Tc(2)^2 / (Pc(2)*100000);
    b = 0.07780*R*Tc(2) / (Pc(2)*100000);
    A = a*PevB*100000/(R*T)^2;
    B = b*PevB*100000/(R*T);
    E = S*(T/Tc(2)/k)^0.5;
    alfa = -1+B;
    beta = A-2*B-3*B^2;
    gamma = -A*B+B^2+B^3;
    coeff =[ 1
         alfa
         beta 
         gamma];
    Z = roots(coeff); %calcola le radici del polinomio
    Z = sort(Z);      %ordina in modo crescente le soluzioni
    ZV = Z(3);
    logphiV = ZV-1-A/(2*sqrt(2)*B)*log((ZV+B*(1+sqrt(2)))/(ZV+B*(1-sqrt(2))))-log(ZV-B);
    phi0B = exp(logphiV);

% L V xA yA
tentativo1 = [FA FB zA zB];
sol1=fsolve(@fesame2,tentativo1,options);

PevA
PevB
phiA
phiB
phi0A
phi0B
L = sol1(1)
V = sol1(2)
xA = sol1(3)
xB = 1-xA
yA = sol1(4)
yB = 1-yA




