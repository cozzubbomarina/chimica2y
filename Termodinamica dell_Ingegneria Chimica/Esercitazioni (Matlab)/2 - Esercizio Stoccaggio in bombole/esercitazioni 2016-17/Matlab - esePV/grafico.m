function Dg=grafico(PV_calc)

global a b R T
Peq=PV_calc*1e5; %Bar -> Pa
v_ar=[];
P_ar=[5,6,7,8,9,10,27,70,100,550]*1e5; %Bar -> Pa
P_ar=[P_ar Peq];

P_ar=sort(P_ar);


%Calcolo della v per i diversi valori di P

for k=1:1:length(P_ar)
    P=P_ar(k);

%calcolo A e B 
A = (a*P)/((R*T)^2);
B = (b*P)/(R*T);

%parametri cubica
alf = -1+B;
bet = A-2*B-3*(B^2);
gam = -A*B+(B^2)+(B^3);

%Soluzione cubica

Z = roots([1 alf bet gam]);

ZR = [];
for i = 1:3
   if isreal(Z(i))
   	ZR = [ZR Z(i)];   
   end
end
sort(ZR);
Zl = min(ZR);   
Zv = max(ZR);

%Calcolo v

if P < Peq
    v= (Zv*R*T)/P;
    v_ar=[v_ar v];
end
if P > Peq
    v= (Zl*R*T)/P;
    v_ar=[v_ar v];
end

if P == Peq
v1= (Zv*R*T)/P;
    v_ar=[v_ar v1];
    
v2= (Zl*R*T)/P;
    v_ar=[v_ar v2];
    
end
end
P_ar=[P_ar Peq];
P_ar=sort(P_ar);

P_ar=P_ar.*1e-5; %Pa -> Bar
v_ar=v_ar.*1000;

Dg = [P_ar' v_ar'];


