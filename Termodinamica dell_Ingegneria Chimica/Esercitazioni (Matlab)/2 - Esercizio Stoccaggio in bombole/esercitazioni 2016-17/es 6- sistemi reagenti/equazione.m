function f=equazione(lambda)

global KT P n0 v

n=n0+lambda*v;
x=n/(sum(n)+1);
Pi=P*x;

f=KT-prod(Pi.^v);