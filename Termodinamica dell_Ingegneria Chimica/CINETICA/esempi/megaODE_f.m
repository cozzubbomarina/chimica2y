function f=megaODE_f(tau)
global C0 k1 k2
[t,C]=ode23('megaODE_f2',[0 tau],C0);
f=C(length(t),1)-0.02*C0(1);