function Tbv = tbollavir(T)

global A B C Tc Pc om x p R

PO1=(10^(A(1)+B(1)/(T+C(1))))/760; %p in atm; T in�C
PO2=(10^(A(2)+B(2)/(T+C(2))))/760;

%1
Tr1=(T+273)/Tc(1);%in K
F11=0.0637+0.331/(Tr1^2)-0.423/(Tr1^3)-0.008/(Tr1^8);
F01=0.1445-0.33/Tr1-0.1385/(Tr1^2)-0.0121/(Tr1^3)-0.000607/(Tr1^8);
bv1=F01+om(1)*F11;
Bv1=bv1*R*Tc(1)/Pc(1);
phivap1=exp(PO1*Bv1/(R*(T+273)));%a p0
phi1=exp(p*Bv1/(R*(T+273)));%a p
rphi1=phivap1/phi1; %rapporto tra i coeff phi calcolati a p0(vap) e p, usando la relazione del viriale per cui ln(phi)=B(T)*p/RT
K1=rphi1*PO1/p;

%2
Tr2=(T+273)/Tc(2);
F02=0.1445-0.33/Tr2-0.1385/(Tr2^2)-0.0121/(Tr2^3)-0.000607/(Tr2^8);
F12=0.0637+0.331/(Tr2^2)-0.423/(Tr2^3)-0.008/(Tr2^8);
bv2=F02+om(2)*F12;
Bv2=bv2*R*Tc(2)/Pc(2);
phivap2=exp(PO2*Bv2/(R*(T+273)));
phi2=exp(p*Bv2/(R*(T+273)));
rphi2=phivap2/phi2; %rapporto tra i coeff phi calcolati a p0 e p, usando la relazione del viriale per cui ln(phi)=B(T)*p/RT
K2=rphi2*PO2/p;

Tbv=K1*x(1)+K2*x(2)-1  %in�C
end




