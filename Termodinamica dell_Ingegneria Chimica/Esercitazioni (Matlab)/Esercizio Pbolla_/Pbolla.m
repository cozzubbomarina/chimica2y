function f=Pbolla(X)

global T x Tc Pc om

P=X(1);
y(1)=X(2);
y(2)=1-y(1);
tipo=4;

%fase vapore
[Z,A,B,Ap,Bp,E]=EoS_mix(T,P,y,Tc,Pc,om,tipo);
phi1V=phi_mix(Z(3),A,B,Ap(1),Bp(1),tipo);
phi2V=phi_mix(Z(3),A,B,Ap(2),Bp(2),tipo);

%fase liquida
[Z,A,B,Ap,Bp,E]=EoS_mix(T,P,x,Tc,Pc,om,tipo);
phi1L=phi_mix(Z(1),A,B,Ap(1),Bp(1),tipo);
phi2L=phi_mix(Z(1),A,B,Ap(2),Bp(2),tipo);

f(1)=phi1V*y(1)-phi1L*x(1);
f(2)=phi2V*y(2)-phi2L*x(2);