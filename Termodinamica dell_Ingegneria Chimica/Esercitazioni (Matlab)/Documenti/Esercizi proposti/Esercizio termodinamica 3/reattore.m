function f=reattore(lambda)

global no v K T2 P2 Tc Pc om tipo

n=no+lambda*v;
x=n/sum(n);

[Z,A,B,S,k]=EoS(T2,P2,Tc(1),Pc(1),om(1),tipo);
phiA=phi_puri(Z(3),A,B,tipo);

[Z,A,B,S,k]=EoS(T2,P2,Tc(2),Pc(2),om(2),tipo);
phiB=phi_puri(Z(3),A,B,tipo);

[Z,A,B,S,k]=EoS(T2,P2,Tc(3),Pc(3),om(3),tipo);
phiC=phi_puri(Z(3),A,B,tipo);

a=P2*10^-5*[x(1)*phiA x(2)*phiB x(3)*phiC];

f=K-prod(a.^v);