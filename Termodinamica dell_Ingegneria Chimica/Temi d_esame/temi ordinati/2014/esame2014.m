close all
clear all
clc
format shortg
global n0V n0L nuL p H Keq_VH

%%dati
F1=200; %mol/s
F2=100; %mol/s
%[02 N2 CHO COOH]
zV=[0.21 0.79 0 0];
zL=[0 0 1 0];
n0V=zV*F1;
n0L=zL*F2;
nuL=[-0.5 0 -1 1];
p=5*1e5; %Pa
Tsis=470; %K
Trif=298; %K
prif=1; %bar
R=8.314; %J/mol K
%calcolo Henry per 1 e 2
a=[-658 3351 0 0];
b=[16.984 2.884 0 0];
c=[-0.0269 -0.0163 0 0]; 
H=(a+b.*Tsis+c.*(Tsis)^2)*1e5; %vettore, [Pa] (T in K)

%calcolo Cp
%alfaCp=[4.9 9.1 11.3 27.4]*1e3; 
%betaCp=[0.0015 0.0562 0.0241 0.0987]*1e3;
% CpdiTsis=(alfaCp+betaCp*Tsis)*1e3; %J/mol

%calcolo Keq_VH  
%      [O2 N2 CHO COOH]
G0frif=[0 0 -196.4 -207.2]*1e3;%J/mol
H0frif=[0 0 -229.4 -232.2]*1e3; %J/mol

%Kirkoff: voglio H0RdiT/RT^2 (integranda)PER IPOTESI COST CON LA T
%-->H0R(Tr)=H0R(Trif)
syms t %uso t come variabile dell'integrale
H0Rrif=sum(nuL.*H0frif); %scalare
integranda=H0Rrif/(R*t^2);

%Van't Hoff:
G0Rrif=sum(nuL.*G0frif);%scalare
G0RrifRT=G0Rrif/(R*Trif);
I_sim=int(integranda,t,Trif,Tsis);
I_sim=vpa(I_sim);
I=eval(I_sim);%ti valuta la funzione in quel valore
G0RRT=G0RrifRT-I; %(deltaG0R/RT)
Keq_VH=exp(-G0RRT);

options=optimset('tolfun',1e-10,'maxiter',1e4,'maxfunevals',10000);
l=fsolve(@esame2014_sistema,[1 1 1],options) %cos� risolve tenendo conto delle opzioni che ho definito prima

%% PARTE FACOLTATIVA:
%Kirkoff: voglio H0RdiT/RT^2 (integranda)-->calcolo i cp singoli:
%syms T t %uso T come variabile del primo integrale e t del secondo
%CpdiT=(alfaCp+betaCp*T); %vettore [CpO2 CpN2 CpCOH CpCOOH] funz di T
%DeltaCpdiT=sum(nuL.*CpdiT) %funzione di T, scalare
%H0Rrif=sum(nuL.*H0frif); %scalare
%H0Rdit=H0Rrif+int(DeltaCpdiT,T,Trif,t); %funzione scalare di t, da integrare in Van'tHoff (H0R/RT^2)
%integranda=H0Rdit/(R*t^2);
 