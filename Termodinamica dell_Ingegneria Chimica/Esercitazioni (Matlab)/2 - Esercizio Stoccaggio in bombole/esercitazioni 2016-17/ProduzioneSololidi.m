%% Produzione di solidi
clear all
close all
clc

%% DATI
R=8.314;
F1=50/3600;              %mol/h
T1=50+273.15;       %K
w=[0.1 0.2 0.7 0];  %frazione molare in F1
Tr=75+273.15;       %K
Pr=1;               %atm
Ts=11+273.15;
Ps=1;               %atm
nu=[-1 -1 0 1];
cp=[7 7.5 8 9]*4.186;
antP=[7.2 -1600 210];
%% Calcolo della costante di equilibrio
%  alla temperatura del reattore
syms T
DH0R=20*4186;
Keq=vpa(0.5*exp(int(DH0R/(R*T^2),298,Tr)));

%% Calcolo della composizione uscente dal reattore
syms l
n0=F1*w;
n=n0+nu*l;
nt=sum(n);
z=n/nt;
sol=solve(prod(z.^nu)==Keq,l);
l=min(vpa(sol));
n=n0+nu*l;
nt=sum(n);
z=(n/nt)'
F2=F1-l;
%ytot=sum(y)

% CHECK
if sum(z)==1
    display('sommatoria frazioni molari -> corretta')
else
    display('sommatoria frazioni molari -> non corretta')
end

%% Calcolo potenza reattore
Q=(sum(F1*w.*cp*(Tr-T1)))+l*(DH0R)
%% Separatore di fase
%% SolubilitÓ solido
P0PL=10^(antP(1)+antP(2)/(Ts-273.15+antP(3))); %torr
P0PS=0.01;  %torr
yP=vpa(P0PS/P0PL);

%% Composizione uscente dal separatore
S=F2*(z(4)-yP)/(1-yP)
L=F2-S
y(1:3)=F2*z(1:3)/L;
y(4)=yP;
y'
%ytot=sum(y)
% CHECK
if sum(y)==1.0
    display('sommatoria frazioni molari -> corretta')
else
    display('sommatoria frazioni molari -> non corretta')
end
