clc; clear all; close all;

% eseguito con delta=6

global P Pr nu1 nu2 keq1TR keq2TR dh0f R T Tr Pr  cp n0z n0m P0 z m
delta=6;
R=8.314; %J/mol/K
dh0f=[10 10 20 20 30]*1e3; %J/mol @298K
cp=[80 90 100 90 130]; %J/mol/K
dhev=[20 22 25 30 62]*1e3; %J/mol @298K, P0
P0=[0.1 0.05 0.3 4 2]; %bar
cl=[90 130 110 130 150]; %J/mol/K
DG0R1=-9e3; %J/mol @298K
DG0R2=-4e3; %J/mol @298K
Pr=1; %bar
P=(1+delta/10); %bar
T=323.15; %K
Tr=298; %K
k=2e-1; %mol/m3/s
Vb=1; %m3
trxn=3600; %s
za=0.5;
zb=0.5;
n0z=1e3; %mol
na=za*exp(-k*Vb*trxn/n0z)*1e3;
z=[na na 1000-2*na 0 0]/1e3; %[A B C D E]
m=[0 0 0 1 0];
n0m=1e3; %mol
nu1=[-1 -1 2 0 0];
nu2=[0 0 -1 -1 1];
keq1TR=exp(-DG0R1/(R*Tr));
keq2TR=exp(-DG0R2/(R*Tr));
DH01=sum(nu1.*dh0f);
DH02=sum(nu2.*dh0f);
DG01=DG0R1/R/Tr-(-(DH01-sum(nu1.*cp*Tr))/R*(1/T-1/Tr)+sum(nu1.*cp)/R*log(T/Tr));
DG02=DG0R2/R/Tr-(-(DH02-sum(nu2.*cp*Tr))/R*(1/T-1/Tr)+sum(nu2.*cp)/R*log(T/Tr));

% TD
sol0=[200 900 900 1100 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2];
options=optimset('MaxFunEvals',50000,'MaxIter',50000,'TolX',1e-8,'TolFun',1e-8);
sol=fsolve(@sistema,sol0,options)

function f=sistema(k)
global P Pr nu1 nu2 keq1TR keq2TR dh0f R T Tr Pr  cp n0z n0m P0 z m
lam1=k(1); lam2=k(2); nV=k(3); nL=k(4); x=k(5:9); y=k(10:14); 

f(1:5)=n0z*z+n0m*m-nL*x-nV*y+nu1*lam1+nu2*lam2;
f(6)=sum(x)-1;
f(7)=sum(y)-1;
f(8:12)=P0.*x-P*y;
f(13)=prod((P*y/Pr).^nu1)-keq1TR*exp(1/R*sum(nu1.*(dh0f/Tr*(1-Tr/T)+cp*log(T/Tr)-cp/Tr*(1-Tr/T))));
f(14)=prod((P*y/Pr).^nu2)-keq2TR*exp(1/R*sum(nu2.*(dh0f/Tr*(1-Tr/T)+cp*log(T/Tr)-cp/Tr*(1-Tr/T))));
end

% f(13)=(y(3)^2/(y(1)*y(2)))-keq1TR*exp(1/R*sum(nu1.*(dh0f/Tr*(1-Tr/T)+cp*log(T/Tr)-cp/Tr*(1-Tr/T))));%exp(-DG01/(R*T)); 
% f(14)=(y(5)/(y(3)*y(4))*Pr/P)-keq2TR*exp(1/R*sum(nu2.*(dh0f/Tr*(1-Tr/T)+cp*log(T/Tr)-cp/Tr*(1-Tr/T))));%exp(-DG02/(R*T));

% %keq1TR*exp(1/R*sum(nu1.*(dh0f/Tr*(1-Tr/T)+cp*log(T/Tr)-cp/Tr*(1-Tr/T))));
% f(2)=y(5)/(y(3)*y(4))*Pr/P-exp(-DG02/R/T); %keq2TR*exp(1/R*sum(nu2.*(dh0f/Tr*(1-Tr/T)+cp*log(T/Tr)-cp/Tr*(1-Tr/T))));




