%% ESAME 04/07/2017

clear all
close all
clc

Tin=423.15; %K
S0=0.657895; %[mol/s]
A0=0.192308; %[mol/s]
B0=0.104167; %[mol/s]
R=8.314; %[J/mol*K]

syms lambda1 lambda2 T

P0a=(12.926-1872.45/(T-25.16))/760;  %P0i in [bar]
P0b=(17.9899-3877.65/(T-45.15))/760;
P0c=(15.8441-2582.32/(T-51.56))/760;
P0d=(18.3036-3816.89/(T-46.13))/760;

deltaG0R1=-14530+86*(T)+R*T*log((P0c *(P0b)^2)/P0a); %P0i in [bar] %T in [�C]
deltaG0R2=-15770+112*(T)+R*T*log(P0d/(P0b*P0c));

gammab=exp(1.5*((lambda1-lambda2)/(A0+B0+S0+2*lambda1-lambda2))^2);
gammac=exp(1.5*((B0+2*lambda1-lambda2)/(A0+B0+S0+2*lambda1-lambda2))^2);

Keq1=exp(-deltaG0R1/(R*T));
Keq2=exp(-deltaG0R2/(R*T));

%EQUAZIONI Keq
eq1=((lambda1-lambda2)*gammac *((B0+2*lambda1-lambda2)*gammab)^2)/(A0-lambda1)*(A0+B0+S0+2*lambda1-lambda2)-Keq1;
eq2=(lambda2*(A0+B0+S0+2*lambda1-lambda2))/(gammab*(B0+2*lambda1-lambda2)*(lambda1-lambda2))-Keq2;

syms T2
intA0=int(4914.38,T2,T,Tin);  %cp in [J/mol*K]
intB0=int(9765.94,T2,T,Tin);
intS0=int(8685.95,T2,T,Tin);

syms T3
intA=int(4914.36,T3,298,T);
intB=int(9765.94,T3,298,T);
intC=int(49817.6,T3,298,T);
intD=int(32236.4,T3,298,T);

%deltaH0fl in [J/mol] si riporta direttamente il numero
eq3=A0*intA0+B0*intB0+S0*intS0+lambda1*(-316043+intA)+(lambda2-2*lambda1)*(158608+intB)+(lambda2-lambda1)*(88450.2+intC)-lambda2*(-241951+intD);

sol=solve(eq1,eq2,eq3,lambda1,lambda2,T);
lambda1=sol.lambda1;
lambda1=double(lambda1);
lambda2=sol.lambda2;
lambda2=double(lambda2);
Treattore=sol.T;
Treattore=double(Treattore);

disp(['La temperatura del reattore � ',num2str(Treattore),'K']);
disp(['Il grado di avanzamento 1 � ',num2str(lambda1)]);
disp(['Il grado di avanzamento 2 � ',num2str(lambda2)]);