function C=ProductMatrix(A,B)

[nA mA]=size(A); %detect the size of matrix A
[nB mB]=size(B);  %detect the size of matrix B

if mA==nB   %logical test to check the compatibility of the two sizes
    for i=1:nA    %cicle that scans the index of the rows of A
        for k=1:mB   %cicle that scans the index of the columns of B
            
            
            sum=0.;  %being C(ij) equal to the sum of the products of the elements of the row i of A and of the column j of B a variable sum is set to 0
            
            for h=1:mA
                sum=sum+A(i,h)*B(h,k);  % and all the products are summed together using an internal for cycle
            end 
            
            C(i,k)=sum; % the value of the sum is stored in the i,j element of the variable C
            
        end

    end
    
else  %if the logical test of the if cyclo is false,the function returns an error message
    
    disp('Dimensions don''t match')
end
    