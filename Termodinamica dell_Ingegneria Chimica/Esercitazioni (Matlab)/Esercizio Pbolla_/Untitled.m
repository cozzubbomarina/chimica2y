clear all
clc
format short g

global T x Tc Pc om

%1 acqua, 2 etanolo
Tc=[647.1 513.9];
Pc=[220.55 61.48]*10^5;
om=[0.345 0.645];

T=473;
x(1)=0.85;
x(2)=1-x(1);

Xo=[30e5 0.5];
var=fsolve('Pbolla',Xo)