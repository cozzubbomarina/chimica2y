clear all
clc
format short g

global antA antB antC
%miscela etanolo(1) e benzene(2)

antA(1)=8.04494;
antB(1)=-1554.3;
antC(1)=222.65;

antA(2)=6.90565;
antB(2)=-1211.03;
antC(2)=220.79;

AB=[1 1];
AB=fsolve('margules',AB);
A=AB(1)
B=AB(2)

global gamma1 gamma2 P x

P=400;      %torr
x(1)=0.9;
x(2)=1-x(1);
gamma1=exp(x(2)^2*(A+2*(B-A)*x(1)));
gamma2=exp(x(1)^2*(B+2*(A-B)*x(2)));

Tb=fsolve('Tbolla',100)

Po1=10^(antA(1)+antB(1)/(antC(1)+Tb));
yetanolo=(Po1*x(1)*gamma1)/P