function Dg2=grafico2(PV_calc)

%Questo grafico è identico al primo ma su un intervallo di pressione piu'
%ristretto (fino a 10 bar)


global a b R T
Peq=PV_calc*1e5; %Bar -> Pa
v_ar=[];
P_ar=[];
limitsup=PV_calc*2;
limitinf=PV_calc/2;

%Inizializzazione array Pressioni
for l= limitinf: 0.1: limitsup
Pl=l*1e5;%Bar -> Pa
P_ar=[P_ar Pl];
end
P_ar=[P_ar Peq];
P_ar=sort(P_ar);

%Calcolo della v per i diversi valori di P

for k=1:1:length(P_ar)
    P=P_ar(k);

%calcolo A e B 
A = (a*P)/((R*T)^2);
B = (b*P)/(R*T);

%parametri cubica
alf = -1+B;
bet = A-2*B-3*(B^2);
gam = -A*B+(B^2)+(B^3);

%Soluzione cubica

Z = roots([1 alf bet gam]);

ZR = [];
for i = 1:3
   if isreal(Z(i))
   	ZR = [ZR Z(i)];   
   end
end
sort(ZR);
Zl = min(ZR);   
Zv = max(ZR);

%Calcolo v

if P < Peq
    v= (Zv*R*T)/P;
    v_ar=[v_ar v];
end
if P > Peq
    v= (Zl*R*T)/P;
    v_ar=[v_ar v];
end

if P == Peq
v1= (Zv*R*T)/P;
    v_ar=[v_ar v1];
    
v2= (Zl*R*T)/P;
    v_ar=[v_ar v2];
    
end
end
P_ar=[P_ar Peq];
P_ar=sort(P_ar);

P_ar=P_ar.*1e-5; %Pa -> Bar
v_ar=v_ar.*1000;

Dg2 = [P_ar' v_ar'];


