clear all
clc
format short g

global K

R=8.314;
%P M O
Dgf=[65510 64500 65560]*4.186;
v=[1 -1 0; 0 -1 1];
Dgo=Dgf*v';
K=exp(-Dgo./(R*700))

Xo=[0.2 0.4];
lambda=fsolve('sistema',Xo)

x=[lambda(1) 1-lambda(1)-lambda(2) lambda(2)]