function y=sistema_mix_id_gas_id(l)

global k_fis_all_id F Aria yaria xF a b R T phi_vap_puro_Pev A B P

L=l(1);
V=l(2);
for i=1:4
    xL(i)=l(i+2);
    yV(i)=l(i+6);
end

%--------------------------------------------------------------------------
% PhiV(T,Pev) in mix
%--------------------------------------------------------------------------
amix=(sum(yV.*sqrt(a))).^2;
bmix=sum(yV.*b);
Amix=amix.*P/((R.*T).^2);
Bmix=bmix.*P./(R.*T);
alfa=-1;
beta=(Amix-Bmix-Bmix.^2);
gamma=(-Amix.*Bmix);
coeff=[1 alfa beta gamma];

z_vap_mix=max(real(roots(coeff)));

phi_vap_mix=exp((B./Bmix).*(z_vap_mix-1)+(Amix./Bmix).*((B./Bmix)-2.*sqrt(A./Amix)).*log((z_vap_mix+Bmix)/z_vap_mix)-log(z_vap_mix-Bmix));

%--------------------------------------------------------------------------
% Gamma(x)
%--------------------------------------------------------------------------

gamma(1)=exp(0.5.*xL(2).^2);
gamma(2)=exp(0.5.*xL(1).^2);

%--------------------------------------------------------------------------
% fattore correttivo phi
%--------------------------------------------------------------------------

correzione=[gamma(1).*phi_vap_puro_Pev(1)./phi_vap_mix(1) gamma(2).*phi_vap_puro_Pev(2)./phi_vap_mix(2) 1 1 ];

k_fis=k_fis_all_id.*correzione;

%--------------------------------------------------------------------------
       
y(1:4)=F.*xF+Aria.*yaria-V.*yV-L.*xL; % 4 BMC
y(5:8)=yV-k_fis.*xL;                  % 4 Eq. L/V
y(9)=sum(yV)-1;                       % Stech. Vap
y(10)=sum(xL)-1;                      % Stech. Liq


