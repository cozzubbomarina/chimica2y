function f=sitema2006(l)
global p0E p G0R1 G0R2 l3
l1=l(1);
l2=l(2);
l3=l(3);
xE=(l2-l3)/(100-l1-l2-l3);

%[A B C D E]
f(1)=xE-p/p0E;
f(2)=exp(-G0R1)-(((l2-l1)*(100-l1-l2-l3))/((35-l1)^2));
f(3)=exp(-G0R2)-(((l2-l3)*(100-l1-l2-l3))/(l1-l2)*(30-l2));
