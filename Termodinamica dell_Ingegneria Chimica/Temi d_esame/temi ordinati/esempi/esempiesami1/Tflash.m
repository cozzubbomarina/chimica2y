 %risolvo problema di rugiada riducendomi a 1 equaz in 1 incognita (T)
 function[f]=Tflash(T)
global y antA antB antC p

%definisco p0 in forma vettoriale
p0=(10.^(antA-(antB./(T-273+antC))))./750; %bar (con T output in K)

%scrivo le equaz da azzerare (una in forma vettoriale)
f=sum((p*y)./p0)-1;