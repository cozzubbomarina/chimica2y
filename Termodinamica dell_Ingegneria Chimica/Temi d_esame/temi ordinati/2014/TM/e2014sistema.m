function f=e2014sistema(h)          %s=[O2v N2v CH3CHO CH3COOH O2l N2l] = [1 2 3 4 5 6]

global ni n0 P H k2

l1=h(1);
l2=h(2);
l3=h(3);

l=[l1;l2;l3];

n=n0'+ni*l;
nv=[n(1) n(2)];
nl=[n(3) n(4) n(5) n(6)];
ntl=sum(nl);
ntv=sum(nv);
x=[0 0 nl/ntl];
y=[nv/ntv 0 0 0 0];

k1=x(4)/(x(3)*sqrt(x(5)*H(1)));

f(1)=k1-k2;
f(2)=H(1)*x(5)-P*y(1);
f(3)=H(2)*x(6)-P*y(2);
