% ESEMPI ESAMI SCRITTI 3
clear all
close all
clc

% DATI
% 1-A 2-B 3-C
R=8.314;%J/molK
T1=350; %K
P1=1;   %bar
T2=400; %K
P2=12;  %bar
F1=100; %mol/s
F2=50;  %mol/s
nu=[-2 -1 1];
Dg0f=[1.223 0.3211 -1.372]*1e3; %J/mol
Dh0f=[1.398 0.8612 -2.132]*1e3; %J/mol
cpA=[11.3 15.4 57.6];   %J/mol/K
cpB=[0.0141 0.0937 0.434];  %J/mol/K^2
Tc=[190.7 417.0 554.0]; %K
Pc=[46.41 77.11 45.60]; %bar
om=[0.290 0.276 0.294]; %-

% Calcolo delle entalpie residue
T=[T1 T2];
Tr=T/Tc(1);
P=[P1 P2];
Pc=[46.41 77.11 45.60];
s=[0.37464 1.54226 -0.26992];
S=s(1)+om(1)*s(2)+s(3)*om(1)^2;
k=(1+S*(1-Tr.^1/2)).^2;
a=0.45724*(R*Tc(1))^2*k/Pc(1);
b=0.07780*R*Tc(1)/Pc(1);
A=a.*P./(R*T).^2;
B=b*P./(R*T);
alfa=-1+B;
beta=A-2*B-3*B.^2;
gamma=-A.*B+B.^2+B.^3;
coeff1=[1 alfa(1) beta(1) gamma(1)];
Z1=roots(coeff1);
Z(1)=max(Z1);
coeff2=[1 alfa(2) beta(2) gamma(2)];
Z2=roots(coeff2);
Z(2)=max(Z2);
E=S*sqrt(Tr./k);
hrRT=Z-1-A./(2*sqrt(2)*B).*(1+E).*log((Z+B*(1+sqrt(2))./(Z+B*(1-sqrt(2)))));
hr=hrRT*R.*T;
syms T
W=F1*(int(cpA(1)+cpB(1)*T,T,T1,T2)+hr(2)-hr(1));
W=vpa(W)

F3=F1+F2;
z(1)=F1/F3;
z(2)=F2/F3;
n0=[F3*z(1) F3*z(2) 0];








