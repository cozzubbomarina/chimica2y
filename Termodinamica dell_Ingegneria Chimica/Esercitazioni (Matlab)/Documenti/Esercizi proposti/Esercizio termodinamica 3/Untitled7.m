clear all
clc
format short g

global no v K T2 P2 Tc Pc om tipo
R=8.314;

F1=100;
F2=50;
P1=1e5;
T1=350;
P2=12e5;
T2=400;

Tc=[190.7 417 554];
Pc=[46.41 77.11 45.6]*10^5;
om=[0.29 0.276 0.294];

Dgf=[1223 321.1 -1372];
Dhf=[1398 861.2 -2132];
cpai=[11.3 15.4 57.6];
cpbi=[0.0141 0.0937 0.434];

%2A+B-->C
v=[-2 -1 1];
no=[F1 F2 0];

tipo=4;
[Z,A,B,S,k]=EoS(T1,P1,Tc(1),Pc(1),om(1),tipo);
E=S*sqrt(T1/(Tc(1)*k));
[hrRT,srR]=fr(Z(3),A,B,E,tipo);
hr1=hrRT*R*T1;

[Z,A,B,S,k]=EoS(T2,P2,Tc(1),Pc(1),om(1),tipo);
E=S*sqrt(T2/(Tc(1)*k));
[hrRT,srR]=fr(Z(3),A,B,E,tipo);
hr2=hrRT*R*T2;

w=quad(@(t) cpai(1)+cpbi(1)*t, T1,T2)+hr2-hr1

Dho=Dhf*v';
Dgo=Dgf*v';
cpa=cpai*v';
cpb=cpbi*v';

Ko=exp(-Dgo/(R*298));
K=Ko*exp( quad(@(t) (Dho+cpa*(t-298)+cpb/2*(t.^2-298^2))./(R*t.^2), 298, T2) )

lambda=fsolve('reattore', 40)
n=no+lambda*v
x=n/sum(n)