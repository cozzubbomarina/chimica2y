function [Tb]=Tbolla2(T)

global antA antB antC gamma1 gamma2 P x K %sono tutti numeri

%funz da azzerare: p0(T)*x*gamma=p*y  con incognite y e T

p01=10^(antA(1)+antB(1)/(antC(1)+T)); %scrivo qui tutto ci� che dipende da T e che entra nella funz da azzerare
p02=10^(antA(2)+antB(2)/(antC(2)+T));

K(1)=p01*gamma1/P;
K(2)=p02*gamma2/P;

%inserisco tutto nella stechiometrica e riduco il sistema a un'equazione
%nell'incognita T, che chiamo 'Tb': Tb=(p01*x(1)*gamma1+p02*x(2)*gamma2)/P-1;

Tb=K(1)*x(1)+K(2)*x(2)-1;
end

