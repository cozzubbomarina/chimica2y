% Funzione da azzerare dal main
function f=Temaesame2015_f(X)
global F z nu Keqs P1 Dh0f cpA cpB T0
T1=X(1);
l=X(2);
n=F*z+nu*l;
nt=sum(n);
x=n/nt;
Keq=eval(Keqs);
f(1)=1/P1*(x(4)/(x(1)*x(2)))-Keq;
f(2)=F*sum(z.*(Dh0f+cpA*(T1-T0)+cpB/2*(T1^2-T0^2)))+l*sum(nu.*Dh0f);
