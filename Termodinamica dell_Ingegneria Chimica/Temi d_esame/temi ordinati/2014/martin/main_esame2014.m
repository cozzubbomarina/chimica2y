clear all
close all
clc
format shortg

global Trz P F1 F2 zV R HO2 HN2 DG0rz

%%dati   %approssimazione gas perfetto e miscela ideale
 Trz=470;%K
 P=5*10^5;%Pa
 F1=200;%mol/s
 F2=100;
 zV=[0.21 0.79 0 0]; %[O2 N2 CHO COOH]
 nu=[-0.5 0 -1 1];
 R=8.314; %J/K*mol 
 
 DG0rifd= [ 0 0 -196.4 -207.2].*10^3;
 DH0rifd= [ 0 0 -229.4 -232.2].*10^3;
 HO2d=[-6.58 16.084 -0.0269];
 HN2d= [3351 2.884 -0.163];
 cpA=[4.9 9.1 11.3 27.4].*10^3;
 cpB=[0.0015 0.0562 0.0241 0.0987].*10^3;
 
 HO2=(HO2d(1)+HO2d(2)*Trz+HO2d(3)*Trz^2)*10^5;
 HN2=(HN2d(1)+HN2d(2)*Trz+HN2d(3)*Trz^2)*10^5;
 
 syms T
 
 cp(1)=cpA(1)+cpB(1)*T;
 cp(2)=cpA(2)+cpB(2)*T;
 cp(3)=cpA(3)+cpB(3)*T; 
 cp(4)=cpA(4)+cpB(4)*T;
 
 DG0rif=sum(nu.*DG0rifd);
 DH0rif=sum(nu.*DH0rifd);
 
 cprz=sum(nu.*cp);
 
 syms T1
 
 DH0rz=DH0rif+int(cprz,T,298,T1);
 
I_sim=int(-DH0rz/(R*T1),T1,298,Trz);
I=eval(I_sim);
DG0rz=DG0rif+I;
  
 
x= fsolve(@esame2014,[150 150 0.5 0.5 0.1 0.1 0.5 0.5 50]);
 
 
 
 
 
 
 