clc; clear all; close all;

global nu dg0f dh0f R Tf cl P0 Pf na nb 
Ta=410; %K 
Tb0=340;
Tb=580;
Tf=345;
Pab=5; %bar
Pf=1;
na=2; %kmol/h
nb=1;
cl=[41.7 39.5]; %J/kmol
dh0f=[-31780 -13880]*1e3; %@ (L) 298K 1bar
dg0f=[-21650 -12400]*1e3;
A=[8.07131 8.08097]; B=[1730.630 1582.271]; C=[233.426 239.726];
T=Tf-273.15;
P0=10.^(A-B./(T+C))*133.3/1e5; %bar 
nu=[-1 2];
R=8314; %J/kmol/K

%soluzione
sol0=[0.5 0.5 0.5 0.5 1.5 1.5 0.5];
options=optimset('MaxFunEvals',30000,'MaxIter',30000,'TolX',1e-7,'TolFun',1e-7);
sol=fsolve(@sistema,sol0,options)

function f=sistema(m)
global nu dg0f dh0f R Tf cl P0 Pf na nb 
x=m(1:2); y=m(3:4); lam=m(5); V=m(6); L=m(7); 

Trif=298; %K
keqrif=exp(-(sum(nu.*dg0f))/(R*Trif));

f(1)=sum(x-y);
f(2:3)=P0.*x-Pf*y;
f(4)=na-L*x(1)-V*y(1)-lam;
f(5)=nb-L*x(2)-V*y(2)+2*lam;
f(6)=na+nb-L-V+sum(nu.*lam);
f(7)=x(2)^2/x(1)-keqrif*exp(1/R*(1/Trif-1/Tf)*(sum(nu.*dh0f)+sum(nu.*cl*Trif))+1/R*(sum(nu.*cl))*log(Tf/Trif));
end



