function eq=eqchimico(lambda)
global n0 nu P2 G0RRT y
%definisco le frazioni molari di gas in funz di lambda:
n=n0+lambda*nu; %vettore
ntot=sum(n0); %scalare
y=n/ntot; %vettore

%attivit�:
produttoria=prod((P2*y).^(nu)); %BOOO?
Keq=produttoria; %FUNZIONE DI LAMBDA

%eq chimico:
eq=log(Keq)+G0RRT;
