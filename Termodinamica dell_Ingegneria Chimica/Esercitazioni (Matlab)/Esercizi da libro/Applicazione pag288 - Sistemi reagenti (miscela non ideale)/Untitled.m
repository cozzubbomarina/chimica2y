clear all
clc
format short g

global T P no v K Tc Pc om

T=548;
P=35; %bar

R=8.314;
%etilene+acqua-->etanolo
v=[-1 -1 1];
no=[1 5 0];

Tc=[282.3 647.1 513.9];
Pc=[50.40 220.55 61.48]*10^5;
om=[0.087 0.345 0.645];

Dhf=[52510 -241818 -235100];
Dgf=[68460 -228572 -168490];

Dho=Dhf*v';
Dgo=Dgf*v';
Ko=exp(-Dgo/(R*298))
K=Ko*exp(Dho/R*(1/298-1/T))

lambda=fsolve('equazione',0.5)
n=no+lambda*v;
x=n/sum(n)