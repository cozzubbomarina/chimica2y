clc
close all
clear all
global T P Trif Prif dhf dgr1 dgr2 cp R nui1 nui2 Pi0 n1 n2 zi wi
T=50+273.25; %K
P=1; %bar
Prif=1; %bar
Trif=298; %K
dhf=[10 10 20 20 30]*1e3; %j/mol A B C D E
cp=[80 90 100 90 130]; %j/molK
dgr1=-9e3; %j/mol
dgr2=-4e3; %j/mol
R=8.314; %j/molK
nui1=[-1 -1 2 0 0];
nui2=[0 0 -1 -1 1];
Pi0=[0.1 0.05 0.3 4 2];
n1=1e3;
n2=1e3;
zi=[0.2433 0.2433 0.5133 0 0];
wi=[0 0 0 1 0];

x0=[200 900 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 500 500];
options=optimset('MaxFunEval',20000,'TolX',1e-8,'TolFun',1e-8,'MaxIter',20000);
sol=fsolve(@sistema,x0,options)

function y=sistema(x)
global T P Trif Prif dhf dgr1 dgr2 cp R nui1 nui2 Pi0 n1 n2 zi wi
lam1=x(1);
lam2=x(2);
yi=[x(3) x(4) x(5) x(6) x(7)];
xi=[x(8) x(9) x(10) x(11) x(12)];
n3=x(13); n4=x(14);
kTr1=exp(-(dgr1)/(R*Trif));
kTr2=exp(-(dgr2)/(R*Trif));

y(1)=kTr1.*exp(sum(nui1.*(dhf./R*(1/Trif-1/T)+cp./R*log(T/Trif)-cp.*Trif/R*(1/Trif-1/T))))-...
    prod((P*yi./Prif).^nui1);
y(2)=kTr2.*exp(sum(nui2.*(dhf./R*(1/Trif-1/T)+cp./R*log(T/Trif)-cp.*Trif/R*(1/Trif-1/T))))-...
    prod((P*yi./Prif).^nui2);
y(3)=n1+n2-n3-n4+sum(nui1*lam1+nui2*lam2);
y(4:8)=n1*zi+n2*wi-n3*xi-n4*yi+nui1*lam1+nui2*lam2;
y(9:13)=P*yi-Pi0.*xi;
y(14)=sum(xi-yi);
end


