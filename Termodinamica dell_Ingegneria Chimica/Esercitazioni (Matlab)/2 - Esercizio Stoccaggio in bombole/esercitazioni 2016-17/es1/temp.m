function f=temp(T3)

global Tc Pc om sr2 tipo
R=8.314;
T2=298; P2=1.01325e5;
P3=220*1.01325e5;

[Z,A,B,S,k]=EoS(T3,P3,Tc,Pc,om,tipo);
E=S*sqrt(T3/(Tc*k));
[hrRT,srR]=fr(Z(3),A,B,E,tipo);
sr3=srR*R;

I=quad(@(T) (4.196+154.565e-3*T-81.076e-6*T.^2+16.813e-9*T.^3)./T , T2,T3);

f=I-R*log(P3/P2)+sr3-sr2;